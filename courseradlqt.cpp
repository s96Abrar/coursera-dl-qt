/*
    coursera-dl-qt
    A GUI utility for coursera-dl
    Copyright (C) 2020  Abrar

    This program is free software: you can redistribute it and/or modify
    it under the terms of the GNU General Public License as published by
    the Free Software Foundation, either version 3 of the License, or
    (at your option) any later version.

    This program is distributed in the hope that it will be useful,
    but WITHOUT ANY WARRANTY; without even the implied warranty of
    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
    GNU General Public License for more details.

    You should have received a copy of the GNU General Public License
    along with this program.  If not, see <http://www.gnu.org/licenses/>.
*/

#include <QDebug>
#include <QFileDialog>
#include <QMessageBox>


#include "courseradlqt.h"
#include "ui_courseradlqt.h"

courseradlqt::courseradlqt(QWidget *parent)
    : QWidget(parent)
    , ui(new Ui::courseradlqt)
{
    ui->setupUi(this);

    QFile file("/usr/bin/coursera-dl");
    if (!file.exists()) {
        QMessageBox::critical(this, "Program not found", "Coursera-dl is not installed. Please install coursera-dl then run this program.");
        close();
    }

    sett = new QSettings("minicube", "coursera-dl-qt");
    ui->email->setText(sett->value("email").toString());
    ui->password->setText(sett->value("password").toString());
    ui->cauth->setPlainText(sett->value("cauth").toString());

    proc = new QProcess;

    connect(ui->saveCredential, &QToolButton::clicked, [&]() {
        sett->setValue("email", ui->email->text());
        sett->setValue("password", ui->password->text());
        sett->setValue("cauth", ui->cauth->toPlainText());
    });

    connect(proc, static_cast<void(QProcess::*)(int, QProcess::ExitStatus)>(&QProcess::finished),
    [ = ](int exitCode, QProcess::ExitStatus exitStatus) {
        Q_UNUSED(exitCode)

        if (exitStatus == QProcess::NormalExit) {
            ui->status->setText("Course Downloaded");
        } else {
            ui->status->setText("Problem occurred");
        }

        toggle(true);
    });

    connect(proc, &QProcess::readyReadStandardError, [&]() {
        ui->output->appendPlainText(proc->readAllStandardError());
    });

    connect(ui->email, &QLineEdit::textChanged, [&](const QString &arg1) {
        emptyField(arg1, ui->email);
    });

    connect(ui->password, &QLineEdit::textChanged, [&](const QString &arg1) {
        emptyField(arg1, ui->password);
    });

    connect(ui->courseName, &QLineEdit::textChanged, [&](const QString &arg1) {
        emptyField(arg1, ui->courseName);
    });

    connect(ui->downloadPath, &QLineEdit::textChanged, [&](const QString &arg1) {
        emptyField(arg1, ui->downloadPath);
    });

    connect(ui->cauth, &QPlainTextEdit::textChanged, [&]() {
        emptyField(ui->cauth->toPlainText(), ui->cauth);
    });
}

courseradlqt::~courseradlqt()
{
    delete ui;
}

void courseradlqt::on_showPassword_clicked(bool checked)
{
    if (checked) {
        ui->showPassword->setIcon(QIcon::fromTheme("password-show-off"));
        ui->password->setEchoMode(QLineEdit::Password);
    } else {
        ui->showPassword->setIcon(QIcon::fromTheme("password-show-on"));
        ui->password->setEchoMode(QLineEdit::Normal);
    }
}

void courseradlqt::on_browse_clicked()
{
    QString folder = QFileDialog::getExistingDirectory(this, "Select download path", QDir::homePath());
    if (!folder.count()) {
        qDebug() << "ERROR: Empty folder selected.";
        return;
    }

    ui->downloadPath->setText(folder);
}

void courseradlqt::on_download_clicked()
{
    if (!ui->email->text().count()) {
        qDebug() << "ERROR: Empty email...";
        return;
    }

    if (!ui->password->text().count()) {
        qDebug() << "ERROR: Empty password...";
        return;
    }

    if (!ui->cauth->toPlainText().count()) {
        qDebug() << "ERROR: Empty CAUTH...";
        return;
    }

    if (!ui->courseName->text().count()) {
        qDebug() << "ERROR: Empty course name...";
        return;
    }

    if (!ui->downloadPath->text().count()) {
        qDebug() << "ERROR: Empty download path...";
        return;
    }

    toggle(false);

    ui->status->setText("Downloading...");

    QStringList args;
    args << "--subtitle-language" << "en";
    args << "--download-quizzes";
    args << "--mathjax-cdn" << "https://cdn.bootcss.com/mathjax/2.7.1/MathJax.js";
    args << "--cache-syllabus";
    args << "--verbose-dirs";
    args << "-u" << ui->email->text();
    args << "-p" << ui->password->text();
    args << "-ca" << ui->cauth->toPlainText();
    args << "--path" << ui->downloadPath->text();
    args << ui->courseName->text();

    proc->setProgram("coursera-dl");
    proc->setArguments(args);
    proc->start();
}

void courseradlqt::toggle(bool state)
{
    ui->email->setEnabled(state);
    ui->password->setEnabled(state);
    ui->showPassword->setEnabled(state);
    ui->cauth->setEnabled(state);
    ui->saveCredential->setEnabled(state);
    ui->courseName->setEnabled(state);
    ui->downloadPath->setEnabled(state);
    ui->download->setEnabled(state);
    ui->download->setEnabled(state);
}

template <class T>
void courseradlqt::emptyField(const QString &arg1, T *w)
{
    QString empty = QString("%1#%2 {"
                    "border: 1px solid red"
                    "}").arg(w->metaObject()->className()).arg(w->objectName());

    if (arg1.count()) {
        w->setStyleSheet("");
    } else {
        w->setFocus();
        w->setStyleSheet(empty);
    }
}
