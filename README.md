# coursera-dl-qt

A GUI app to download courses from online course platform [Coursera](https://coursera.org) based on the command line utility of [coursera-dl](https://github.com/coursera-dl/coursera-dl). 

<img src="sc-coursera-dl-qt.png">

## Features 

Things that is included-

* You can save your password and email for not entering this multiple times.
> Use the saving feature on your own risk cause this will be saved into `.config` folder so that anyone can see your credential.
* You can have verbose output of downloading.

## In future

Upcoming features-

* Saving email and credential as encrypted.
* Batch downloading of courses.

# Dependency

* qt5-base
* [coursera-dl](https://github.com/coursera-dl/coursera-dl/)

# Build 

Build coursera-dl-qt from source.

## For Arch user

```sh
# Install git and Qt dependency
sudo pacman -S git qt5-base

# To Install or build `coursera-dl` follow coursera-dl installation instructions.

# Clone the repository
git clone https://gitlab.com/s96Abrar/coursera-dl-qt
cd coursera-dl-qt

# Build the source
qmake -qt5 && make

# Run coursera-dl gui
./coursera-dl-qt

# To install coursera-dl gui
sudo make install
```

## For debian user

```sh
# Install git
sudo apt-get install git

# Install Qt dependency
sudo apt-get install qtbase5-dev

# To Install or build `coursera-dl` follow coursera-dl installation instructions.

# Clone the repository
git clone https://gitlab.com/s96Abrar/coursera-dl-qt
cd coursera-dl-qt

# Build the source
qmake -qt5 && make

# Run coursera-dl gui
./coursera-dl-qt

# To install coursera-dl gui
sudo make install
```