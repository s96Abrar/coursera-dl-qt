QT       += widgets

TEMPLATE = app
TARGET = coursera-dl-qt
VERSION = 1.0.0

# Disable warnings, enable threading support and c++11
CONFIG  += thread silent build_all c++11

# Build location
isEmpty( BUILD_PREFIX ) {
        BUILD_PREFIX = ./build
}

MOC_DIR       = $$BUILD_PREFIX/moc/
OBJECTS_DIR   = $$BUILD_PREFIX/obj/
RCC_DIR       = $$BUILD_PREFIX/qrc/
UI_DIR        = $$BUILD_PREFIX/uic/

unix {
        isEmpty(PREFIX) {
                PREFIX = /usr
        }

        target.path = $$PREFIX/bin/

        desktop.path  = $$PREFIX/share/applications/
        desktop.files = coursera-dl-qt.desktop

        icons.path    = /usr/share/icons/hicolor/scalable/apps/
        icons.files   = coursera.png

        INSTALLS     += target icons desktop
}

SOURCES += \
    main.cpp \
    courseradlqt.cpp

HEADERS += \
    courseradlqt.h

FORMS += \
    courseradlqt.ui

RESOURCES += \
    resource.qrc
