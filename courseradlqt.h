/*
    coursera-dl-qt
    A GUI utility for coursera-dl
    Copyright (C) 2020  Abrar

    This program is free software: you can redistribute it and/or modify
    it under the terms of the GNU General Public License as published by
    the Free Software Foundation, either version 3 of the License, or
    (at your option) any later version.

    This program is distributed in the hope that it will be useful,
    but WITHOUT ANY WARRANTY; without even the implied warranty of
    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
    GNU General Public License for more details.

    You should have received a copy of the GNU General Public License
    along with this program.  If not, see <http://www.gnu.org/licenses/>.
*/

#pragma once

#include <QWidget>
#include <QProcess>
#include <QSettings>

QT_BEGIN_NAMESPACE
namespace Ui {
    class courseradlqt;
}
QT_END_NAMESPACE

class courseradlqt : public QWidget {
    Q_OBJECT

public:
    courseradlqt(QWidget *parent = nullptr);
    ~courseradlqt();

private slots:
    void on_showPassword_clicked(bool checked);
    void on_browse_clicked();
    void on_download_clicked();

private:
    Ui::courseradlqt *ui;
    QProcess *proc;
    QSettings *sett;

    template< class T>
    void emptyField(const QString &arg1, T *w);

    void toggle(bool state);
};
